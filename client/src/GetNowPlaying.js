import React, { Component } from 'react';
import SpotifyWebApi from 'spotify-web-api-js';
const spotifyApi = new SpotifyWebApi();

class GetNowPlaying extends Component {

    constructor() {
        super();
        const params = this.getHashParams();
        const token = params.access_token;
        if (token) {
            spotifyApi.setAccessToken(token);
        }
        this.state = {
            nowPlaying: { name: '', albumArt: '', artist: '', url: '', urlToShare: '' },
        }
    }

    getHashParams() {
        var hashParams = {};
        var e, r = /([^&;=]+)=?([^&;]*)/g,
            q = window.location.hash.substring(1);
        e = r.exec(q)
        while (e) {
            hashParams[e[1]] = decodeURIComponent(e[2]);
            e = r.exec(q);
        }
        return hashParams;
    }

    getNowPlaying() {
        spotifyApi.getMyCurrentPlaybackState()
            .then((response) => {
                try {
                    this.setState({
                        nowPlaying: {
                            name: response.item.name,
                            albumArt: response.item.album.images[0].url,
                            artist: response.item.artists[0].name,
                            url: response.item.album.external_urls.spotify,
                            urlToShare: 'https://twitter.com/intent/tweet?url=' + response.item.album.external_urls.spotify + '&via=CopifySpotify&text=Estoy%20escuchando%20' + encodeURIComponent(response.item.name) + '%20de%20' + encodeURIComponent(response.item.artists[0].name) + '&hashtags=copify'
                        }
                    });
                } catch (e) {
                    console.log('Undefined value!');
                }
            })
    }

    render() {
        this.getNowPlaying()
        if (this.state.nowPlaying.name === '') {
            return (
                <div>
                    <div>
                        <h1 className="titulo">REPRODUCIENDO</h1>
                        <h6 className="subtitulo">Actualmente no estás reproduciendo nada. </h6>
                    </div>
                </div>
            )
        } else {
            return (
                <div>
                    <div>
                        <h1 className="titulo">REPRODUCIENDO</h1>
                        <h6 className="subtitulo">Actualmente reproduciendo:  {this.state.nowPlaying.name}</h6>
                    </div>
                    <div>
                        <img className="row col-sm-12 px-0 image-center" alt="" src={this.state.nowPlaying.albumArt} />
                    </div>
                    <div>
                        <h6 className="subtitulo">¡Compártelo con tus amigos!</h6>
                        <div className="centrar">
                            <a target="_blank" href={this.state.nowPlaying.urlToShare}>
                                <img className="boton_redondo" alt="" src="https://www.shareicon.net/data/256x256/2017/06/22/887584_logo_512x512.png"></img>
                            </a>
                        </div>
                    </div>
                </div>
            )
        }
    };
}

export default GetNowPlaying;





