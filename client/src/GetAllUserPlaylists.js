import React, { useState, useEffect } from 'react';
import Dropdown from './Dropdown';
import Listbox from './Listbox';
import Detail from './Detail';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.css';
import './styles.css'

const GetAllUserPlaylists = () => {

  function getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);
    e = r.exec(q)
    while (e) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
      e = r.exec(q);
    }
    return hashParams;
  }
  const params = getHashParams();
  const token = params.access_token;

  const [playlist, setPlaylist] = useState({ selectedPlaylist: '', listOfPlaylistFromAPI: [] });
  const [tracks, setTracks] = useState({ selectedTrack: '', listOfTracksFromAPI: [] });
  const [trackDetail, setTrackDetail] = useState(null);

  useEffect(() => {

    axios('https://api.spotify.com/v1/me/playlists?', {
      method: 'GET',
      headers: { 'Authorization': 'Bearer ' + token },
      json: true
    })
      .then(playlistsResponse => {
        setPlaylist({
          selectedPlaylist: playlist.selectedPlaylist,
          listOfPlaylistFromAPI: playlistsResponse.data.items
        })
      });

  }, [playlist.selectedPlaylist, token]);

  const buttonClicked = e => {
    e.preventDefault();

    axios(`https://api.spotify.com/v1/playlists/${playlist.selectedPlaylist}/tracks?limit=20`, {
      method: 'GET',
      headers: { 'Authorization': 'Bearer ' + token }
    })
      .then(tracksResponse => {
        setTracks({
          selectedTrack: tracks.selectedTrack,
          listOfTracksFromAPI: tracksResponse.data.items
        })
      });
  }

  const playlistChanged = val => {
    console.log(val);
    setPlaylist({
      selectedPlaylist: val,
      listOfPlaylistFromAPI: playlist.listOfPlaylistFromAPI
    });
  }

  const listboxClicked = val => {
    const currentTracks = [...tracks.listOfTracksFromAPI];
    const trackInfo = currentTracks.filter(t => t.track.id === val);
    setTrackDetail(trackInfo[0].track);
  }

  return (
    <div className="container">
      <h1 className="titulo">MIS PLAYLISTS</h1>
      <h6 className="subtitulo">Ve las canciones de tus playlists</h6>
      <form onSubmit={buttonClicked}>
        <Dropdown label="Playlists" options={playlist.listOfPlaylistFromAPI} selectedValue={playlist.selectedPlaylist} changed={playlistChanged} />
        <div className="col-sm-6 row form-group px-0">
          <button type='submit' className="btn col-sm-12 buscar" >
            Buscar
          </button>
        </div>
        <div className="row">
          <Listbox items={tracks.listOfTracksFromAPI} clicked={listboxClicked} />
          {trackDetail && <Detail {...trackDetail} />}
        </div>
      </form>
    </div>
  );
}

export default GetAllUserPlaylists;
