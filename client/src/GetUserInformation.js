import React, { Component, useState } from 'react';
import SpotifyWebApi from 'spotify-web-api-js';
import 'bootstrap/dist/css/bootstrap.css';

const spotifyApi = new SpotifyWebApi();
class App extends Component {

  constructor() {
    super();
    const params = this.getHashParams();
    const token = params.access_token;
    if (token) {
      spotifyApi.setAccessToken(token);
    }
    this.state = {
      loggedIn: token ? true : false,
      currentUser: { id: 'none', name: '', email: 'none', img: 'http://www.sportsmallgroup.com/wp-content/uploads/2019/08/Default-User-Avatar.png', country: 'none', followers: 'none', type: 'none' },
      nowPlaying: { name: 'Not Checked', albumArt: '' }
    }
  }

  getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);
    e = r.exec(q)
    while (e) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
      e = r.exec(q);
    }
    return hashParams;
  }

  getCurrentUserId() {
    spotifyApi.getMe()
      .then((response) => {
        this.setState({
          currentUser: {
            id: response.id,
          }
        });
        localStorage.setItem("id_actual", response.id);
      })
  }

  getUserProfile() {
    spotifyApi.getMe()
      .then((response) => {
        this.setState({
          currentUser: {
            id: response.id,
            name: response.display_name,
            email: response.email,
            img: response.images[0].url,
            country: response.country,
            followers: response.followers.total,
            type: response.type,
          }
        });
      })
  }

  render() {
    let contador = 0;
    if (contador === 0) {
      this.getUserProfile();
      contador += 1;
    }
    if (this.state.loggedIn && contador !== 0) {
      return (
        <div className="row_user">
          <div className="col-md-12 row form-group px-0">
            <h1 className="nombre_p"> Hola {this.state.currentUser.name}</h1>
            <img alt="imagen del usuario" className="user_img_p" src={this.state.currentUser.img}></img>
            <div className="informacion_p">
              <p className="infoP"><span>Email asociado:</span> {this.state.currentUser.email}</p>
              <p className="infoP"><span>País:</span> {this.state.currentUser.country}</p>
              <p className="infoP"><span>Seguidores:</span> {this.state.currentUser.followers}</p>
              <p className="infoP"><span>Tipo de usuario:</span> {this.state.currentUser.type}</p>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default App;