import registerServiceWorker from './registerServiceWorker';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import * as ReactBootstrap from 'react-bootstrap';

import Login from './Login.js';
import GetNowPlaying from './GetNowPlaying.js';
import GetTopSongsByCategories from './GetTopSongsByCategories';
import GetUserInformation from './GetUserInformation.js';
import GetAllUserPlaylists from './GetAllUserPlaylists.js'

import SpotifyWebApi from 'spotify-web-api-js';
const spotifyApi = new SpotifyWebApi();

function getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    e = r.exec(q)
    while (e) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
        e = r.exec(q);
    }
    return hashParams;
}

function renderAllUserPlaylists() {
    return (
        ReactDOM.render(<GetAllUserPlaylists />, document.getElementById('view'))
    );
}

function renderTopSongsByCategories() {
    return (
        ReactDOM.render(<GetTopSongsByCategories />, document.getElementById('view'))
    );
}


function renderUserInformation() {
    return (
        ReactDOM.render(<GetUserInformation />, document.getElementById('view'))
    );
}


function renderNowPlaying() {
    return (
        ReactDOM.render(<GetNowPlaying />, document.getElementById('view'))
    );
}

function render() {
    if (state.loggedIn) {
        return (
            ReactDOM.render(
                <ReactBootstrap.Navbar className="nav-react" collapseOnSelect expand="lg">
                    <img className="logo-sp" src="https://cdn.iconscout.com/icon/free/png-256/spotify-3212920-2676917.png"></img>
                    <ReactBootstrap.Navbar.Brand className="nav-react-logo">Copify</ReactBootstrap.Navbar.Brand>
                    <ReactBootstrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <ReactBootstrap.Navbar.Collapse id="responsive-navbar-nav">
                        <ReactBootstrap.Nav className="mr-auto">
                            <button className="nav-item" id="AllUserPlaylistsbtn" onClick={renderAllUserPlaylists}>Mis playlists</button>
                            <button className="nav-item" id="TopSongsByCategoriesbtn" onClick={renderTopSongsByCategories}>Tendencias</button>
                            <button className="nav-item" id="NowPlayingbtn" onClick={renderNowPlaying}>Reproduciendo ahora</button>
                        </ReactBootstrap.Nav>

                        <ReactBootstrap.Nav>
                            <button className="nav-item" id="UserInformationbtn" onClick={renderUserInformation}>Mi perfil</button>
                            <a className="nav-item nosubrayar" target="_blank" href="https://twitter.com/CopifySpotify/">Nuestro Twitter</a>
                        </ReactBootstrap.Nav>
                    </ReactBootstrap.Navbar.Collapse>
                </ReactBootstrap.Navbar>,
                document.getElementById('navbar')),
            ReactDOM.render(<GetUserInformation />, document.getElementById('view'))

        )
    } else {
        return (
            ReactDOM.render(<Login />, document.getElementById('view'))
        )
    }
}

//main
const params = getHashParams();
const token = params.access_token;
if (token) {
    spotifyApi.setAccessToken(token);
}
var state = {
    loggedIn: token ? true : false
}

render();
registerServiceWorker();

